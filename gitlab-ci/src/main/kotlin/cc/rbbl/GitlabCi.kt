package cc.rbbl

import pcimcioch.gitlabci.dsl.gitlabCi

fun main() {
    gitlabCi(validate = true, "../.gitlab-ci.yml") {
    }
}