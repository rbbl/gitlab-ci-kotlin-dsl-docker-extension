import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    `maven-publish`
    signing
    id("org.jetbrains.dokka") version "1.7.20"
}

group = "cc.rbbl"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.github.pcimcioch:gitlab-ci-kotlin-dsl:1.3.2")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val dokkaJar by tasks.creating(Jar::class) {
    archiveClassifier.set("javadoc")
    from(tasks.dokkaJavadoc)
}


java {
    withSourcesJar()
    withJavadocJar()
}

publishing {
    publications.create<MavenPublication>("Lib") {
        artifact(dokkaJar)
        pom {
            name.set("Gitlab CI Kotlin DSL - Docker Extension")
            description.set("A small Library to manage Docker build and publish Tasks within the Gitlab CI Kotlin DSL")
            url.set("https://www.rbbl.cc/projects")
            licenses {
                license {
                    name.set("MIT License")
                    url.set("https://gitlab.com/rbbl/gitlab-ci-kotlin-dsl-docker-extension/-/blob/master/LICENSE")
                }
            }
            developers {
                developer {
                    id.set("rbbl-dev")
                    name.set("rbbl-dev")
                    email.set("dev@rbbl.cc")
                }
            }
            scm {
                url.set("https://gitlab.com/rbbl/gitlab-ci-kotlin-dsl-docker-extension")
            }
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/41165903/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
        maven {
            name = "Snapshot"
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials(PasswordCredentials::class)
        }
        maven {
            name = "Central"
            url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials(PasswordCredentials::class)
        }
    }
}

signing {
    val signingKey: String? by project
    val signingPassword: String? by project
    useInMemoryPgpKeys(signingKey, signingPassword)
    setRequired({
        gradle.taskGraph.allTasks.filter { it.name.endsWith("ToCentralRepository") }.isNotEmpty()
    })
    sign(publishing.publications)
}